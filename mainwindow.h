#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QAudioRecorder>
#include <QUrl>
#include <QMediaPlayer>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QAudioRecorder *audioRecorder;

    QMediaPlayer *mediaPlayer;

private slots:
    void on_pushButton_startRec_clicked();
    void on_pushButton_stopRec_clicked();
    void on_pushButton_play_clicked();
};

#endif // MAINWINDOW_H
