#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //  create recorder  //
    audioRecorder = new QAudioRecorder(this);

    //  configure recorder  //
    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/PCM");
    audioSettings.setQuality(QMultimedia::HighQuality);
    audioRecorder->setEncodingSettings(audioSettings);

    //  set destination  //
    audioRecorder->setOutputLocation(QUrl::fromLocalFile("/home/mfbmir/samp.wav"));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_startRec_clicked()
{
    //  start rec  //
    audioRecorder->record();

    qDebug() << "Recording started";
}

void MainWindow::on_pushButton_stopRec_clicked()
{
    //  stop rec  //
    audioRecorder->stop();

    qDebug() << "Recording stoped";
}

void MainWindow::on_pushButton_play_clicked()
{
    mediaPlayer = new QMediaPlayer(this);
    mediaPlayer->setMedia(QUrl::fromLocalFile("/home/mfbmir/samp.wav"));
    mediaPlayer->setVolume(50);
    mediaPlayer->play();
}
